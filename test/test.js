const { factorial, div_check } = require('../src/util.js');
const { expect, assert } = require('chai');

describe('test_fun_factorials', () => {

	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		assert.equal(product, 1);
	})

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	})

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		assert.equal(product, 3628800);
	})
});

describe('test_div_check', () => {

	it('div_check_49_returns_true', () => {
		const quotient = div_check(49);
		assert.equal(quotient, true);
	})

	it('div_check_105_returns_true', () => {
		const quotient = div_check(105);
		expect(quotient).to.equal(true);
	})

	it('div_check_700_returns_true', () => {
		const quotient = div_check(700);
		assert.equal(quotient, true);
	})

	it('div_check_9_returns_false', () => {
		const quotient = div_check(9);
		expect(quotient).to.equal(false);
	})
});